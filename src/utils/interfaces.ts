interface IResponseObject {
    data: any;
    error: any;
}

interface IAggregateInstance {
    tx_id: string,
    _id: string,
    _version: number
}

interface IHashObject {
    id: string,
    model: string,
    version: number
}

export { IResponseObject, IAggregateInstance, IHashObject }