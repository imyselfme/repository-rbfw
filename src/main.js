const App = require('./lib/app')

App.listen(3000, (error) => {
    if(error) console.log('[HTTP] An error as ocurred while starting http server', error)
    else console.log('[HTTP] Server is connected at port', 3000)
})

module.exports = App