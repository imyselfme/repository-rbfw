import { TransactionHandler } from './transactions'
import { IResponseObject, IAggregateInstance } from '../utils/interfaces'

class ResponseObject implements IResponseObject{
    constructor(public error: any = null, public data: any = null) {
		this.error = error
        this.data = data
    }
}

module.exports = function(app: any)
{
	var extractHeaders = function(req: any) {
		return {
			txid: req.get('rbfw-transaction-id'),
			model: req.get('rbfw-model')
		}
	}

	var findHandler = async (req: any, res: any) => {
		var { txid, model } = extractHeaders(req)
		var response: IResponseObject;

		try{
			response = new ResponseObject(null, await app.locals.TransactionHandler.find(req.body, model, txid))
			res.status(200)
		}
		catch(error) {
			response = new ResponseObject(error.message, null)
			res.status(400)
		}

		res.json(response)
	}

	app.locals.TransactionHandler = new TransactionHandler();


    app.post('/findByID', async (req: any, res: any) => {
		var { txid, model } = extractHeaders(req)
		var response: IResponseObject;

		try{
			response = new ResponseObject(null, await app.locals.TransactionHandler.findByID(req.body, model, txid))
			res.status(200)
		}
		catch(error) {
			response = new ResponseObject(error.message, null)
			res.status(400)
		}

		res.json(response)
	})

	app.post('/findOne', async (req: any, res: any) => {
		var { txid, model } = extractHeaders(req)
		var response: IResponseObject;

		try{
			response = new ResponseObject(null, await app.locals.TransactionHandler.findOne(req.body, model, txid))
			res.status(200)
		}
		catch(error) {
			response = new ResponseObject(error.message, null)
			res.status(400)
		}

		res.json(response)
	})

	app.post('/find', async (req: any, res: any) => {
		var { txid, model } = extractHeaders(req)
		var response: IResponseObject;

		try{
			response = new ResponseObject(null, await app.locals.TransactionHandler.find(req.body, model, txid))
			res.status(200)
		}
		catch(error) {
			response = new ResponseObject(error.message, null)
			res.status(400)
		}

		res.json(response)
	})

    app.post('/save', async (req: any, res: any) => {
		var { txid, model } = extractHeaders(req)
		var response: IResponseObject;

		try{
			response = new ResponseObject(null, await app.locals.TransactionHandler.save(req.body, model, txid))
			res.status(201)
		}
		catch(error) {
			response = new ResponseObject(error.message, null)
			res.status(400)
		}

		res.json(response)
	})

	app.get('/validateModel/:modelName', (req: any, res: any) => {
		var availableModels = app.locals.TransactionHandler.registeredModels
		var exists: boolean;

		if(availableModels.some((m: string) => m == req.params.modelName)) exists = true
		else exists = false
		
		res.json(new ResponseObject(null, exists)).end()
	})

	return app
}