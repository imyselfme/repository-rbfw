import _ from 'underscore'
import { MongoData } from './mongo'
import { Logs } from './logs';

const path = require('path');
var modelsPath: string = path.join(__dirname, '../models');

interface ICacheObject {
    originalTx: string
    model: string
    data: any
}

class TransactionHandler {

    private _uncommitedData: Array<ICacheObject>
    private _logs: Logs
    private _dbAdapter: any;

    constructor() {
        this._uncommitedData = []
        this._logs = new Logs()
        this._dbAdapter = new MongoData(modelsPath);
    }

    get registeredModels() {
        return this._dbAdapter.models
    }

    public async findByID(query: any, model: string, txid: string) {
        let agg: any

        try {
            let cache = this.findInCache(query, model)

            if(!cache) {
                agg = await this._dbAdapter.findByID(model, query)

                //Check if findOne() returned an empty document
                if(Object.keys(agg).length > 0) {
                    this._logs.updateVersionLog(agg._id, agg._version)
                    this._logs.updateTransactionState(txid, 'inactive')
                }

                return agg
            }
            else {
                this._logs.updateTransactionState(txid, 'active')

                let { originalTx, data } = cache[0]

                if(originalTx != txid)
                    this._logs.updateTransactionDependencies([originalTx], txid)

                return data
            }
        }
        catch(error) { throw error }
    }

    public async findOne(query: any, model: any, txid: string) {
        let agg: any

        try {
            let cache = this.findInCache(query, model)

            if(!cache) {
                agg = await this._dbAdapter.findOne(model, query)

                //Check if findOne() returned an empty document
                if(Object.keys(agg).length > 0) {
                    this._logs.updateVersionLog(agg._id, agg._version)
                    this._logs.updateTransactionState(txid, 'inactive')
                }

                return agg
            }
            else {
                this._logs.updateTransactionState(txid, 'active')

                let { originalTx, data } = cache[0]

                if(originalTx != txid)
                    this._logs.updateTransactionDependencies([originalTx], txid)

                return data
            }
        }
        catch(error) { throw error }
    }

    public async find(query: any, model: string, txid: string) {
        let aggs: any

        try {
            let cache = this.findInCache(query, model)

            if(!cache) {
                aggs = await this._dbAdapter.find(model, query)

                if(aggs.length > 0) {
                    aggs.forEach((ag: any) => this._logs.updateVersionLog(ag._id, ag._version))
                    this._logs.updateTransactionState(txid, 'inactive')
                }

                return aggs
            }
            else {
                this._logs.updateTransactionState(txid, 'active')

                let aux = cache.filter((co: any) => co.originalTx != txid)
                .map((co: any) => { return co.originalTx })

                //Filter duplicates; Same transaction may have updated several documents
                aux = _.uniq(aux)

                this._logs.updateTransactionDependencies(aux, txid)

                return cache.map((c: any) => { return c.data })
            }
        }
        catch(error) { throw error }
    }

    public async save(agg: any, model: string, txid: string) {
        try {
            //Update aggregate version based on _versionLog
            agg._version = this._logs.updateVersionLog(agg._id)

            //Change Tx state to 'update'
            this._logs.updateTransactionState(txid, 'update')

            //Store @agg on _uncommitedData
            var ud: ICacheObject = { originalTx: txid, model, data: agg }
            this._uncommitedData.push(ud)

            return ud.data;
        }
        catch(error) { throw error }
    }

    public async commit() {
        //Persist @_uncommitedData to database
        try {
            //Store in hashtable with address @txid and push { aggid, version } to body
            //Store @agg: IAggregateInstance in _uncommitedData
            //return await this._dbAdapter.save(aggid, agg, model)
        }
        catch(error) { throw error }
    }

    public rollback(txid: string) {
        //Rollback transaction @txid
        //Delete objects from @_uncommitedData corresponding to @txid
    }

    private findInCache(query: any, model: string) {
        let queryEntries = Object.entries(query)
        let hit = undefined, result = []

        //Filter cache uncommited data by matching the key/value pairs in @query 
        hit = _.filter(this._uncommitedData, (ud: any) => {
            let checks = queryEntries.length

            if(ud.model != model)
                return undefined

            for(let e in queryEntries) {
                if(ud.data[e[0]] == e[1])
                    checks--
            }

            return checks == 0? ud:undefined
        })

        if(!hit || !hit.length)
            return undefined

        //Group cache hits by _id
        let groups = _.groupBy(hit, (ud: any) => { return ud.data._id })

        //Fetch latest version for each group
        for(let uds of Object.values(groups)) {
            uds = _.sortBy(uds, '_version')
            result.push(uds[uds.length-1])
        }

        return !result.length? undefined:result
    }
}

export { TransactionHandler }