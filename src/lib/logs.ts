const HashMap = require('hashmap')
import inc from '../utils/increment'


class Transaction {
    dependencies: Array<string>
    private _validStates: Array<string> = ['inactive', 'active', 'update', 'commit', 'rollback']

    constructor(private _state: string) {
        if(!this.validateState(_state))
            throw new Error('Invalid transaction state')

        this.dependencies = new Array<string>()
    }

    get state() { return this._state }

    set state(state: string) {
        if(!this.validateState(state))
            throw new Error('Invalid transaction state')
        
        this._state = state
    }

    private validateState(state: string) {
        return this._validStates.includes(state)
    }
}

class Logs {
    private _transactionLog: any
    private _versionLog: any

    constructor() {
        this._transactionLog = new HashMap()
        this._versionLog = new HashMap()
    }

    public updateVersionLog(id: any, version?: number) {
        let v;
        let sid = new String(id)

        if(version != undefined) {
            v = version
            this._versionLog.set(sid.valueOf(), version)
        }  
        else {
            v = inc(this._versionLog.get(sid.valueOf()))
            this._versionLog.set(sid.valueOf(), v)
            console.log('v', sid.valueOf(), this._versionLog.get(sid.valueOf()))

        }

        return v
    }

    public updateTransactionState(txid: string, state: string) {
        console.log('upTL', txid, state)
        let tx: Transaction

        if(!this._transactionLog.has(txid)) {
            tx = new Transaction(state)
        }
        else {
            tx = this._transactionLog.get(txid)
            tx.state = state
        }

        this._transactionLog.set(txid, tx)
    }

    public updateTransactionDependencies(txs: Array<string>, dep: string) {
        for(let tx of txs) {
            let t = this._transactionLog.get(tx)
            if(!t.dependencies.includes(dep))
                t.dependencies.push(dep)

            this._transactionLog.set(tx, t)
        }
    }
}

export { Logs }