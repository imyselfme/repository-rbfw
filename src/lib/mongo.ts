const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');

export class MongoData {

    private _models: any;

    public constructor(modelsPath: string) {
        this._models = {};
        var modelsFiles = fs.readdirSync(modelsPath);

        for (var i in modelsFiles)
        {
            var modelSchema = require(path.join(modelsPath, modelsFiles[i]));
            modelSchema.path('_version', { type: Number, default: 0, required: true })
            var modelName = modelsFiles[i];

            modelName = modelName.substring(0, modelName.length - 3);
            this._models[modelName] = mongoose.model(modelName, modelSchema);
        }

        if (mongoose.connection.readyState == 0)
        {
            var mongo_url = process.env.npm_package_config_MONGO_URL;
            mongoose.connect(mongo_url,  { useNewUrlParser: true })
            .then(() => console.log('[MONGO] Connection established to host', mongo_url))
            .catch((err: any) => console.log('[MONGO] Connection error', err))
        }
    }

    get models() {
        return Object.keys(this._models);
    }

    public async findByID(modelName: string, id: string) {
        let model = this._models[modelName]
        if(!model) throw new Error('Invalid Model name')

        var oid = new mongoose.Types.ObjectId(id)
        var query = model.findById(oid)

        return query.exec();
    }

    public async findOne(modelName: string, criteria: string) {
        let model = this._models[modelName]
        if(!model) throw new Error('Invalid Model name')

        var query = model.findOne(criteria)

        return query.exec();
    }

    public async find(modelName: string, criteria: string) {
        let model = this._models[modelName]
        if(!model) throw new Error('Invalid Model name')

        var query = this._models[modelName].find(criteria) //returns array

        return query.exec();
    }

    public async save(data: any, modelName: string) {
        let model = this._models[modelName]
        if(!model) throw new Error('Invalid Model name')

        var oid = new mongoose.Types.ObjectId(data._id)
        var query = this._models[modelName].findOneAndUpdate(oid, { $set: data }, { new: true })

        return query.exec();
    }
}