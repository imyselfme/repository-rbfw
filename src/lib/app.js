const express = require('express');
const bodyParser = require('body-parser');

function startExpress() {

	var app = express();

	process.on('uncaughtException', function(err)
	{
		console.error('uncaughtException in server will restart process!');
		console.error('uncaughtException in server.js ', err.stack);
		process.exit(1);
	})

	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));

	//Avoid CORS in clients
	app.use(function(req, res, next)
	{	
		console.log('[HTTP] Request:', req.method + ' ' + req.url);

		res.header("Access-Control-Allow-Origin", "*");
		res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
		res.header("Access-Control-Allow-Credentials", "true");

		if ('OPTIONS' === req.method) {
			//respond with 200
			res.status(200);
			res.json();
		}
		else {
			next();
		}
	});
	
	require('./routes')(app)

	//Catch 404 and forward to error handler
	app.use(function(req, res, next)
	{
		var err = new Error('Not Found 404');
		err.status = 404;
		next(err);
	});

	//Error handler
	app.use(function(err, req, res, next)
	{
		//Set locals, only providing error in development
		res.locals.message = err.message;
		res.locals.error = req.app.get('env') === 'development' ? err : {};

		// render the error page
		res.status(err.status || 509);
		console.log('[APP] Error handler ', err);

		var data = {
			error: true,
			status: 509,
			data:
			{
				message: err.message
			}
		}

		res.json(data);
	});

	return app
}

module.exports = startExpress()