const amqp = require('amqplib');

const QueueNames = ['query', 'save', 'rollback']

var amqpUrl = process.env.npm_package_config_AMQP_URL;
var amqpConn: any = null;
var amqpChannel: any = null;

function start() {
    try{
        amqpConn = startAMQP();
        amqpChannel = openChannel();
        assertQueues();
    }
    catch(err) {
        console.log(err)
    }
}

async function startAMQP() {
    try{
        var conn = await amqp.connect(amqpUrl)

        conn.on('error', (err: any) => console.log('[AMQP] Connection error', err.message))
        conn.on('close', () => console.log('[AMQP] Connection closed'))
    }
    catch(err) {
        throw '[AMQP]' + err.message
    }

    console.log('[AMQP] Connection established with host', amqpUrl)
    return conn;
}

async function openChannel() {
    try {
        var ch = await amqpConn.createConfirmChannel()

        ch.on('error', (err: any) => console.error('[AMQP] Channel error', err.message))
        ch.on('close', () => console.log('[AMQP] Channel closed'))
    }
    catch(err) {
        throw '[AMQP]' + err.message
    }

    return ch;
}

async function assertQueues() {
    try {
        for(var q in QueueNames) {
        }
    }
    catch(err) {}
}

start()
