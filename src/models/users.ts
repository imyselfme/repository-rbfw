var Schema = require('mongoose').Schema;

var UserSchema = new Schema(
{
    email: String,
    username: String,
    password: String,
    firstName: String,
    lastName: String,

});

module.exports = UserSchema;