export default function(num: number | undefined) {
    if(num == undefined) return 0
    return num + 1
}